import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';

import { UserService } from './user.service';

const USER = { email: 'lykhun@gmail.com', ssn: '82.05.06-203.16', nationality: 'BE' };

describe('UserService', () => {
  let service: UserService;
  let testController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(UserService);
    testController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return object with id', () => {
    service.post(USER)
      .subscribe(data => expect(data).toEqual({ ...USER, id: 42 }));
    let req = testController.expectOne(environment.baseAPI + '/user');
    req.flush({ ...USER, id: 42 })
  });
});
