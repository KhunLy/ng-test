import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { RegisterForm } from 'src/app/forms/register.form';
import { Country } from 'src/app/models/country.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  fg!: FormGroup;
  countries!: Country[];

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private toastrService: ToastrService,
    private router: Router,
  ) { 
  }

  ngOnInit(): void {
    this._initForm();
    this.countries = this.activatedRoute.snapshot.data['countries'];
  }

  submit() {
    if(this.fg.valid) {
      this.userService.post(this.fg.value).subscribe({
        next: data => {
          this.toastrService.success('Enregistrement OK');
          this.router.navigateByUrl('/home');
        }, 
        error: error => { 
          this.toastrService.error('Enregistrement KO') 
        }
      });
    }
  }

  private _initForm() {
    this.fg = this.fb.group(RegisterForm);
    const nationalityControl = this.fg.get('nationality');
    const ssnControl = this.fg.get('ssn');
    if(ssnControl && nationalityControl) {
      ssnControl.disable();
      nationalityControl.statusChanges.subscribe(() => {
        if(nationalityControl.value?.toLowerCase() === 'be') {
          ssnControl.enable();
        }
        else {
          ssnControl.disable();
        }
      });
    }
  }
} 
