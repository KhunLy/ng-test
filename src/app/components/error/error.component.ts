import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnChanges {

  @Input()
  control!: AbstractControl | null;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
  }
}
